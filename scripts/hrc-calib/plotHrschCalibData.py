import csv
import numpy
import matplotlib.pyplot as plt
import seaborn
import argparse
import os
import pandas

argparser = argparse.ArgumentParser()
argparser.add_argument('path',help="path containing csv files with data", type=str)
arguments = argparser.parse_args()


filepaths = []

for f in os.listdir(arguments.path):
	if f.endswith(".csv"):
		filepaths.append(os.path.join(arguments.path, f))

outputpath = arguments.path

data = pandas.read_csv(filepaths[0]) #get structure from first file

for index in range(1, len(filepaths)):
	data.append(pandas.read_csv(filepaths[index]), ignore_index=True)

data.sort_values(by=['run_number']) #in case of merging multiple files

def saveBarPlot(data, outputpath, variable):

	figure, axis = plt.subplots()
	seaborn.barplot(x="run_number", y=variable, data=data, yerr=data[variable+"_error"])
	plt.xticks(rotation=90)
	plt.title(variable)
	figure.savefig(os.path.join(outputpath, variable+".pdf"))
	plt.close(figure)


saveBarPlot(data,outputpath, "B00_mean_even")
saveBarPlot(data,outputpath, "B00_rms_even")
saveBarPlot(data,outputpath, "B00_mean_odd")
saveBarPlot(data,outputpath, "B00_rms_odd")

saveBarPlot(data,outputpath, "B01_mean_even")
saveBarPlot(data,outputpath, "B01_rms_even")
saveBarPlot(data,outputpath, "B01_mean_odd")
saveBarPlot(data,outputpath, "B01_rms_odd")

saveBarPlot(data,outputpath, "B02_mean_even")
saveBarPlot(data,outputpath, "B02_rms_even")
saveBarPlot(data,outputpath, "B02_mean_odd")
saveBarPlot(data,outputpath, "B02_rms_odd")

saveBarPlot(data,outputpath, "B03_mean_even")
saveBarPlot(data,outputpath, "B03_rms_even")
saveBarPlot(data,outputpath, "B03_mean_odd")
saveBarPlot(data,outputpath, "B03_rms_odd")

saveBarPlot(data,outputpath, "B10_mean_even")
saveBarPlot(data,outputpath, "B10_rms_even")
saveBarPlot(data,outputpath, "B10_mean_odd")
saveBarPlot(data,outputpath, "B10_rms_odd")

saveBarPlot(data,outputpath, "B11_mean_even")
saveBarPlot(data,outputpath, "B11_rms_even")
saveBarPlot(data,outputpath, "B11_mean_odd")
saveBarPlot(data,outputpath, "B11_rms_odd")

saveBarPlot(data,outputpath, "B12_mean_even")
saveBarPlot(data,outputpath, "B12_rms_even")
saveBarPlot(data,outputpath, "B12_mean_odd")
saveBarPlot(data,outputpath, "B12_rms_odd")

saveBarPlot(data,outputpath, "B13_mean_even")
saveBarPlot(data,outputpath, "B13_rms_even")
saveBarPlot(data,outputpath, "B13_mean_odd")
saveBarPlot(data,outputpath, "B13_rms_odd")

saveBarPlot(data,outputpath, "B20_mean_even")
saveBarPlot(data,outputpath, "B20_rms_even")
saveBarPlot(data,outputpath, "B20_mean_odd")
saveBarPlot(data,outputpath, "B20_rms_odd")

saveBarPlot(data,outputpath, "B21_mean_even")
saveBarPlot(data,outputpath, "B21_rms_even")
saveBarPlot(data,outputpath, "B21_mean_odd")
saveBarPlot(data,outputpath, "B21_rms_odd")

saveBarPlot(data,outputpath, "B22_mean_even")
saveBarPlot(data,outputpath, "B22_rms_even")
saveBarPlot(data,outputpath, "B22_mean_odd")
saveBarPlot(data,outputpath, "B22_rms_odd")

saveBarPlot(data,outputpath, "B23_mean_even")
saveBarPlot(data,outputpath, "B23_rms_even")
saveBarPlot(data,outputpath, "B23_mean_odd")
saveBarPlot(data,outputpath, "B23_rms_odd")





saveBarPlot(data,outputpath, "F10_mean_even")
saveBarPlot(data,outputpath, "F10_rms_even")
saveBarPlot(data,outputpath, "F10_mean_odd")
saveBarPlot(data,outputpath, "F10_rms_odd")

saveBarPlot(data,outputpath, "F11_mean_even")
saveBarPlot(data,outputpath, "F11_rms_even")
saveBarPlot(data,outputpath, "F11_mean_odd")
saveBarPlot(data,outputpath, "F11_rms_odd")

saveBarPlot(data,outputpath, "F12_mean_even")
saveBarPlot(data,outputpath, "F12_rms_even")
saveBarPlot(data,outputpath, "F12_mean_odd")
saveBarPlot(data,outputpath, "F12_rms_odd")

saveBarPlot(data,outputpath, "F13_mean_even")
saveBarPlot(data,outputpath, "F13_rms_even")
saveBarPlot(data,outputpath, "F13_mean_odd")
saveBarPlot(data,outputpath, "F13_rms_odd")

saveBarPlot(data,outputpath, "F20_mean_even")
saveBarPlot(data,outputpath, "F20_rms_even")
saveBarPlot(data,outputpath, "F20_mean_odd")
saveBarPlot(data,outputpath, "F20_rms_odd")

saveBarPlot(data,outputpath, "F21_mean_even")
saveBarPlot(data,outputpath, "F21_rms_even")
saveBarPlot(data,outputpath, "F21_mean_odd")
saveBarPlot(data,outputpath, "F21_rms_odd")

saveBarPlot(data,outputpath, "F22_mean_even")
saveBarPlot(data,outputpath, "F22_rms_even")
saveBarPlot(data,outputpath, "F22_mean_odd")
saveBarPlot(data,outputpath, "F22_rms_odd")

saveBarPlot(data,outputpath, "F23_mean_even")
saveBarPlot(data,outputpath, "F23_rms_even")
saveBarPlot(data,outputpath, "F23_mean_odd")
saveBarPlot(data,outputpath, "F23_rms_odd")