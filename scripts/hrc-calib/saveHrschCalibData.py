import uproot
import csv
import numpy
import matplotlib.pyplot as plt
import seaborn
import argparse
import awkward
import itertools
from scipy.optimize import curve_fit
from scipy import stats
import os
import datetime


argparser = argparse.ArgumentParser()
argparser.add_argument('file',help="file with data", type=str)
argparser.add_argument('treepath',help="path do data inside root file", type=str)
argparser.add_argument('-p',help="produce control plots", action="store_true")
arguments = argparser.parse_args()


outputpath = os.path.abspath(datetime.datetime.now().strftime('%Y_%m_%d__%H_%M_%S'))
if not os.path.isdir(outputpath):
	os.makedirs(outputpath)

plot = arguments.p

def fitFunc(x, mean, sigma):
	return numpy.exp(-1.0 * (x - mean)**2 / (2 * sigma**2) ) / ( sigma*numpy.sqrt(2*numpy.pi) )


def getFitParameters(data, run_number, name, outputpath, plot):

	entries, bins = numpy.histogram(data, bins=120, density=True)
	bin_centers = numpy.array([0.5 * (bins[i] + bins[i+1]) for i in range(len(bins)-1)])

	[mean, sigma], cov = curve_fit(fitFunc, xdata=bin_centers, ydata=entries, p0=[30, 10])
	[mean_error, sigma_error] = numpy.sqrt(numpy.diag(cov))

	if numpy.isnan(mean_error):
		mean_error = 0

	if numpy.isnan(sigma_error):
		sigma_error = 0

	if plot:

		title = "run_"+str(run_number)+"_"+name

		figure, axis = plt.subplots()
		seaborn.distplot(data, kde=False, bins=120, norm_hist=True)

		X = numpy.arange( min(data) - 10, max(data) + 10, 0.001 )
		Y = stats.norm.pdf(X, mean, sigma)

		axis.plot(X, Y, 'r')

		plt.title(title)
		
		figure.savefig(os.path.join(outputpath, title+".pdf"))

		plt.close(figure)

	return [mean, mean_error, sigma, sigma_error]

source_file = uproot.open(arguments.file)[arguments.treepath]

run_numbers_src = source_file["runNumber"].array().astype(int)

BCID_src = source_file["BCID"].array().astype(int)
B00_src = source_file["B00"].array().astype(int)
B01_src = source_file["B01"].array().astype(int)
B02_src = source_file["B02"].array().astype(int)
B03_src = source_file["B03"].array().astype(int)
B10_src = source_file["B10"].array().astype(int)
B11_src = source_file["B11"].array().astype(int)
B12_src = source_file["B12"].array().astype(int)
B13_src = source_file["B13"].array().astype(int)
B20_src = source_file["B20"].array().astype(int)
B21_src = source_file["B21"].array().astype(int)
B22_src = source_file["B22"].array().astype(int)
B23_src = source_file["B23"].array().astype(int)
F10_src = source_file["F10"].array().astype(int)
F11_src = source_file["F11"].array().astype(int)
F12_src = source_file["F12"].array().astype(int)
F13_src = source_file["F13"].array().astype(int)
F20_src = source_file["F20"].array().astype(int)
F21_src = source_file["F21"].array().astype(int)
F22_src = source_file["F22"].array().astype(int)
F23_src = source_file["F23"].array().astype(int)

#sort

run_numbers,BCID,B00,B01,B02,B03,B10,B11,B12,B13,B20,B21,B22,B23,F10,F11,F12,F13,F20,F21,F22,F23 = zip(*sorted(zip(run_numbers_src,BCID_src,B00_src,B01_src,B02_src,B03_src,B10_src,B11_src,B12_src,B13_src,B20_src,B21_src,B22_src,B23_src,F10_src,F11_src,F12_src,F13_src,F20_src,F21_src,F22_src,F23_src), key=lambda x: x[0]))


B00_even = []
B01_even = []
B02_even = []
B03_even = []
B10_even = []
B11_even = []
B12_even = []
B13_even = []
B20_even = []
B21_even = [] 
B22_even = [] 
B23_even = []
F10_even = [] 
F11_even = [] 
F12_even = [] 
F13_even = [] 
F20_even = [] 
F21_even = [] 
F22_even = [] 
F23_even = []

B00_odd = []
B01_odd = []
B02_odd = []
B03_odd = []
B10_odd = []
B11_odd = []
B12_odd = []
B13_odd = []
B20_odd = []
B21_odd = [] 
B22_odd = [] 
B23_odd = []
F10_odd = [] 
F11_odd = [] 
F12_odd = [] 
F13_odd = [] 
F20_odd = [] 
F21_odd = [] 
F22_odd = [] 
F23_odd = []


with open(os.path.join(outputpath, "output.csv"), "w") as opened_file:
	output = csv.writer(opened_file)

	output.writerow(["run_number",
				"B00_mean_even", "B00_mean_even_error", "B00_rms_even", "B00_rms_even_error", "B00_mean_odd", "B00_mean_odd_error", "B00_rms_odd", "B00_rms_odd_error",
				"B01_mean_even", "B01_mean_even_error", "B01_rms_even", "B01_rms_even_error", "B01_mean_odd", "B01_mean_odd_error", "B01_rms_odd", "B01_rms_odd_error",
				"B02_mean_even", "B02_mean_even_error", "B02_rms_even", "B02_rms_even_error", "B02_mean_odd", "B02_mean_odd_error", "B02_rms_odd", "B02_rms_odd_error",
				"B03_mean_even", "B03_mean_even_error", "B03_rms_even", "B03_rms_even_error", "B03_mean_odd", "B03_mean_odd_error", "B03_rms_odd", "B03_rms_odd_error",
				"B10_mean_even", "B10_mean_even_error", "B10_rms_even", "B10_rms_even_error", "B10_mean_odd", "B10_mean_odd_error", "B10_rms_odd", "B10_rms_odd_error",
				"B11_mean_even", "B11_mean_even_error", "B11_rms_even", "B11_rms_even_error", "B11_mean_odd", "B11_mean_odd_error", "B11_rms_odd", "B11_rms_odd_error",
				"B12_mean_even", "B12_mean_even_error", "B12_rms_even", "B12_rms_even_error", "B12_mean_odd", "B12_mean_odd_error", "B12_rms_odd", "B12_rms_odd_error",
				"B13_mean_even", "B13_mean_even_error", "B13_rms_even", "B13_rms_even_error", "B13_mean_odd", "B13_mean_odd_error", "B13_rms_odd", "B13_rms_odd_error",
				"B20_mean_even", "B20_mean_even_error", "B20_rms_even", "B20_rms_even_error", "B20_mean_odd", "B20_mean_odd_error", "B20_rms_odd", "B20_rms_odd_error",
				"B21_mean_even", "B21_mean_even_error", "B21_rms_even", "B21_rms_even_error", "B21_mean_odd", "B21_mean_odd_error", "B21_rms_odd", "B21_rms_odd_error",
				"B22_mean_even", "B22_mean_even_error", "B22_rms_even", "B22_rms_even_error", "B22_mean_odd", "B22_mean_odd_error", "B22_rms_odd", "B22_rms_odd_error",
				"B23_mean_even", "B23_mean_even_error", "B23_rms_even", "B23_rms_even_error", "B23_mean_odd", "B23_mean_odd_error", "B23_rms_odd", "B23_rms_odd_error",
				"F10_mean_even", "F10_mean_even_error", "F10_rms_even", "F10_rms_even_error", "F10_mean_odd", "F10_mean_odd_error", "F10_rms_odd", "F10_rms_odd_error",
				"F11_mean_even", "F11_mean_even_error", "F11_rms_even", "F11_rms_even_error", "F11_mean_odd", "F11_mean_odd_error", "F11_rms_odd", "F11_rms_odd_error",
				"F12_mean_even", "F12_mean_even_error", "F12_rms_even", "F12_rms_even_error", "F12_mean_odd", "F12_mean_odd_error", "F12_rms_odd", "F12_rms_odd_error",
				"F13_mean_even", "F13_mean_even_error", "F13_rms_even", "F13_rms_even_error", "F13_mean_odd", "F13_mean_odd_error", "F13_rms_odd", "F13_rms_odd_error",
				"F20_mean_even", "F20_mean_even_error", "F20_rms_even", "F20_rms_even_error", "F20_mean_odd", "F20_mean_odd_error", "F20_rms_odd", "F20_rms_odd_error",
				"F21_mean_even", "F21_mean_even_error", "F21_rms_even", "F21_rms_even_error", "F21_mean_odd", "F21_mean_odd_error", "F21_rms_odd", "F21_rms_odd_error",
				"F22_mean_even", "F22_mean_even_error", "F22_rms_even", "F22_rms_even_error", "F22_mean_odd", "F22_mean_odd_error", "F22_rms_odd", "F22_rms_odd_error",
				"F23_mean_even", "F23_mean_even_error", "F23_rms_even", "F23_rms_even_error", "F23_mean_odd", "F23_mean_odd_error", "F23_rms_odd", "F23_rms_odd_error",
	])



	current_run_number = run_numbers[0]

	for index, run_number in enumerate(run_numbers):

		if run_number == current_run_number:

			if BCID[index] % 2 == 0:

				B00_even.append(B00[index])
				B01_even.append(B01[index])
				B02_even.append(B02[index])
				B03_even.append(B03[index])
				B10_even.append(B10[index])
				B11_even.append(B11[index])
				B12_even.append(B12[index])
				B13_even.append(B13[index])
				B20_even.append(B20[index])
				B21_even.append(B21[index]) 
				B22_even.append(B22[index]) 
				B23_even.append(B23[index])
				F10_even.append(F10[index]) 
				F11_even.append(F11[index]) 
				F12_even.append(F12[index]) 
				F13_even.append(F13[index]) 
				F20_even.append(F20[index]) 
				F21_even.append(F21[index]) 
				F22_even.append(F22[index]) 
				F23_even.append(F23[index])

			else:

				B00_odd.append(B00[index])
				B01_odd.append(B01[index])
				B02_odd.append(B02[index])
				B03_odd.append(B03[index])
				B10_odd.append(B10[index])
				B11_odd.append(B11[index])
				B12_odd.append(B12[index])
				B13_odd.append(B13[index])
				B20_odd.append(B20[index])
				B21_odd.append(B21[index]) 
				B22_odd.append(B22[index]) 
				B23_odd.append(B23[index])
				F10_odd.append(F10[index]) 
				F11_odd.append(F11[index]) 
				F12_odd.append(F12[index]) 
				F13_odd.append(F13[index]) 
				F20_odd.append(F20[index]) 
				F21_odd.append(F21[index]) 
				F22_odd.append(F22[index]) 
				F23_odd.append(F23[index])

		else: #run number changed

			B00_even_params = getFitParameters(B00_even, current_run_number, "B00_even", outputpath, plot)
			B01_even_params = getFitParameters(B01_even, current_run_number, "B01_even", outputpath, plot)
			B02_even_params = getFitParameters(B02_even, current_run_number, "B02_even", outputpath, plot)
			B03_even_params = getFitParameters(B03_even, current_run_number, "B03_even", outputpath, plot)
			B10_even_params = getFitParameters(B10_even, current_run_number, "B10_even", outputpath, plot)
			B11_even_params = getFitParameters(B11_even, current_run_number, "B11_even", outputpath, plot)
			B12_even_params = getFitParameters(B12_even, current_run_number, "B12_even", outputpath, plot)
			B13_even_params = getFitParameters(B13_even, current_run_number, "B13_even", outputpath, plot)
			B20_even_params = getFitParameters(B20_even, current_run_number, "B20_even", outputpath, plot)
			B21_even_params = getFitParameters(B21_even, current_run_number, "B21_even", outputpath, plot)
			B22_even_params = getFitParameters(B22_even, current_run_number, "B22_even", outputpath, plot)
			B23_even_params = getFitParameters(B23_even, current_run_number, "B23_even", outputpath, plot)
			F10_even_params = getFitParameters(F10_even, current_run_number, "F10_even", outputpath, plot)
			F11_even_params = getFitParameters(F11_even, current_run_number, "F11_even", outputpath, plot)
			F12_even_params = getFitParameters(F12_even, current_run_number, "F12_even", outputpath, plot) 
			F13_even_params = getFitParameters(F13_even, current_run_number, "F13_even", outputpath, plot) 
			F20_even_params = getFitParameters(F20_even, current_run_number, "F20_even", outputpath, plot) 
			F21_even_params = getFitParameters(F21_even, current_run_number, "F21_even", outputpath, plot) 
			F22_even_params = getFitParameters(F22_even, current_run_number, "F22_even", outputpath, plot) 
			F23_even_params = getFitParameters(F23_even, current_run_number, "F23_even", outputpath, plot)

			B00_odd_params = getFitParameters(B00_odd, current_run_number, "B00_odd", outputpath, plot)
			B01_odd_params = getFitParameters(B01_odd, current_run_number, "B01_odd", outputpath, plot)
			B02_odd_params = getFitParameters(B02_odd, current_run_number, "B02_odd", outputpath, plot)
			B03_odd_params = getFitParameters(B03_odd, current_run_number, "B03_odd", outputpath, plot)
			B10_odd_params = getFitParameters(B10_odd, current_run_number, "B10_odd", outputpath, plot)
			B11_odd_params = getFitParameters(B11_odd, current_run_number, "B11_odd", outputpath, plot)
			B12_odd_params = getFitParameters(B12_odd, current_run_number, "B12_odd", outputpath, plot)
			B13_odd_params = getFitParameters(B13_odd, current_run_number, "B13_odd", outputpath, plot)
			B20_odd_params = getFitParameters(B20_odd, current_run_number, "B20_odd", outputpath, plot)
			B21_odd_params = getFitParameters(B21_odd, current_run_number, "B21_odd", outputpath, plot)
			B22_odd_params = getFitParameters(B22_odd, current_run_number, "B22_odd", outputpath, plot)
			B23_odd_params = getFitParameters(B23_odd, current_run_number, "B23_odd", outputpath, plot)
			F10_odd_params = getFitParameters(F10_odd, current_run_number, "F10_odd", outputpath, plot) 
			F11_odd_params = getFitParameters(F11_odd, current_run_number, "F11_odd", outputpath, plot) 
			F12_odd_params = getFitParameters(F12_odd, current_run_number, "F12_odd", outputpath, plot) 
			F13_odd_params = getFitParameters(F13_odd, current_run_number, "F13_odd", outputpath, plot) 
			F20_odd_params = getFitParameters(F20_odd, current_run_number, "F20_odd", outputpath, plot) 
			F21_odd_params = getFitParameters(F21_odd, current_run_number, "F21_odd", outputpath, plot) 
			F22_odd_params = getFitParameters(F22_odd, current_run_number, "F22_odd", outputpath, plot) 
			F23_odd_params = getFitParameters(F23_odd, current_run_number, "F23_odd", outputpath, plot)

				
			output.writerow([current_run_number, 
								B00_even_params[0], B00_even_params[1], B00_even_params[2], B00_even_params[3], B00_odd_params[0], B00_odd_params[1], B00_odd_params[2], B00_odd_params[3],
								B01_even_params[0], B01_even_params[1], B01_even_params[2], B01_even_params[3], B01_odd_params[0], B01_odd_params[1], B01_odd_params[2], B01_odd_params[3],
								B02_even_params[0], B02_even_params[1], B02_even_params[2], B02_even_params[3], B02_odd_params[0], B02_odd_params[1], B02_odd_params[2], B02_odd_params[3],
								B03_even_params[0], B03_even_params[1], B03_even_params[2], B03_even_params[3], B03_odd_params[0], B03_odd_params[1], B03_odd_params[2], B03_odd_params[3],
								B10_even_params[0], B10_even_params[1], B10_even_params[2], B10_even_params[3], B10_odd_params[0], B10_odd_params[1], B10_odd_params[2], B10_odd_params[3],
								B11_even_params[0], B11_even_params[1], B11_even_params[2], B11_even_params[3], B11_odd_params[0], B11_odd_params[1], B11_odd_params[2], B11_odd_params[3],
								B12_even_params[0], B12_even_params[1], B12_even_params[2], B12_even_params[3], B12_odd_params[0], B12_odd_params[1], B12_odd_params[2], B12_odd_params[3],
								B13_even_params[0], B13_even_params[1], B13_even_params[2], B13_even_params[3], B13_odd_params[0], B13_odd_params[1], B13_odd_params[2], B13_odd_params[3],
								B20_even_params[0], B20_even_params[1], B20_even_params[2], B20_even_params[3], B20_odd_params[0], B20_odd_params[1], B20_odd_params[2], B20_odd_params[3],
								B21_even_params[0], B21_even_params[1], B21_even_params[2], B21_even_params[3], B21_odd_params[0], B21_odd_params[1], B21_odd_params[2], B21_odd_params[3],
								B22_even_params[0], B22_even_params[1], B22_even_params[2], B22_even_params[3], B22_odd_params[0], B22_odd_params[1], B22_odd_params[2], B22_odd_params[3],
								B23_even_params[0], B23_even_params[1], B23_even_params[2], B23_even_params[3], B23_odd_params[0], B23_odd_params[1], B23_odd_params[2], B23_odd_params[3],
								F10_even_params[0], F10_even_params[1], F10_even_params[2], F10_even_params[3], F10_odd_params[0], F10_odd_params[1], F10_odd_params[2], F10_odd_params[3],
								F11_even_params[0], F11_even_params[1], F11_even_params[2], F11_even_params[3], F11_odd_params[0], F11_odd_params[1], F11_odd_params[2], F11_odd_params[3],
								F12_even_params[0], F12_even_params[1], F12_even_params[2], F12_even_params[3], F12_odd_params[0], F12_odd_params[1], F12_odd_params[2], F12_odd_params[3],
								F13_even_params[0], F13_even_params[1], F13_even_params[2], F13_even_params[3], F13_odd_params[0], F13_odd_params[1], F13_odd_params[2], F13_odd_params[3],
								F20_even_params[0], F20_even_params[1], F20_even_params[2], F20_even_params[3], F20_odd_params[0], F20_odd_params[1], F20_odd_params[2], F20_odd_params[3],
								F21_even_params[0], F21_even_params[1], F21_even_params[2], F21_even_params[3], F21_odd_params[0], F21_odd_params[1], F21_odd_params[2], F21_odd_params[3],
								F22_even_params[0], F22_even_params[1], F22_even_params[2], F22_even_params[3], F22_odd_params[0], F22_odd_params[1], F22_odd_params[2], F22_odd_params[3],
								F23_even_params[0], F23_even_params[1], F23_even_params[2], F23_even_params[3], F23_odd_params[0], F23_odd_params[1], F23_odd_params[2], F23_odd_params[3]])


			current_run_number = run_number

			B00_even = []
			B01_even = []
			B02_even = []
			B03_even = []
			B10_even = []
			B11_even = []
			B12_even = []
			B13_even = []
			B20_even = []
			B21_even = [] 
			B22_even = [] 
			B23_even = []
			F10_even = [] 
			F11_even = [] 
			F12_even = [] 
			F13_even = [] 
			F20_even = [] 
			F21_even = [] 
			F22_even = [] 
			F23_even = []

			B00_odd = []
			B01_odd = []
			B02_odd = []
			B03_odd = []
			B10_odd = []
			B11_odd = []
			B12_odd = []
			B13_odd = []
			B20_odd = []
			B21_odd = [] 
			B22_odd = [] 
			B23_odd = []
			F10_odd = [] 
			F11_odd = [] 
			F12_odd = [] 
			F13_odd = [] 
			F20_odd = [] 
			F21_odd = [] 
			F22_odd = [] 
			F23_odd = []

			if BCID[index] % 2 == 0:

				B00_even.append(B00[index])
				B01_even.append(B01[index])
				B02_even.append(B02[index])
				B03_even.append(B03[index])
				B10_even.append(B10[index])
				B11_even.append(B11[index])
				B12_even.append(B12[index])
				B13_even.append(B13[index])
				B20_even.append(B20[index])
				B21_even.append(B21[index]) 
				B22_even.append(B22[index]) 
				B23_even.append(B23[index])
				F10_even.append(F10[index]) 
				F11_even.append(F11[index]) 
				F12_even.append(F12[index]) 
				F13_even.append(F13[index]) 
				F20_even.append(F20[index]) 
				F21_even.append(F21[index]) 
				F22_even.append(F22[index]) 
				F23_even.append(F23[index])

			else:

				B00_odd.append(B00[index])
				B01_odd.append(B01[index])
				B02_odd.append(B02[index])
				B03_odd.append(B03[index])
				B10_odd.append(B10[index])
				B11_odd.append(B11[index])
				B12_odd.append(B12[index])
				B13_odd.append(B13[index])
				B20_odd.append(B20[index])
				B21_odd.append(B21[index]) 
				B22_odd.append(B22[index]) 
				B23_odd.append(B23[index])
				F10_odd.append(F10[index]) 
				F11_odd.append(F11[index]) 
				F12_odd.append(F12[index]) 
				F13_odd.append(F13[index]) 
				F20_odd.append(F20[index]) 
				F21_odd.append(F21[index]) 
				F22_odd.append(F22[index]) 
				F23_odd.append(F23[index])

