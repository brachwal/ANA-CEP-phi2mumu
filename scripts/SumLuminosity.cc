#include <iostream>         
#include <fstream>          
#include <sstream>          
#include <TSystem.h>        
#include <TROOT.h>          
#include <TTree.h>          
#include "math.h"           
#include "TFile.h"          
#include <unistd.h>         
                            
                            
using namespace std;        


void SumLuminosity(int year)
{
  stringstream ss;
  ss<<"/eos/lhcb/user/b/brachwal/DATA/Analysis/CEP/LowDiMu/LowDimu_"<<year<<"_test.root";
  TFile *f = new TFile(ss.str().c_str());
  TTree *t = (TTree*)f->Get("GetIntegratedLuminosity/LumiTuple");
  Double_t IntegratedLuminosity;
  t->SetBranchAddress("IntegratedLuminosity", &IntegratedLuminosity);
  
  double sum=0.;

  for(int i=0;i<t->GetEntries();++i)
    {
      t->GetEntry(i);
      sum+=IntegratedLuminosity;
      
    }
  cout<<"Integrated Luminosity: "<<sum<<"  pb"<<endl;
  
  
}
