#include <ROOT/RDataFrame.hxx>
#include <iostream>
#include <string>

/*
	A script to filter and reduce NTuple size for further analysis.

*/

const std::string filter = "nVeloTracks < 3 && mum_isMuon && mup_isMuon";


void FilterPreSelection(std::string filename){


	std::cout << filename << std::endl;

	ROOT::RDataFrame file("dimu/dimu", filename);
	auto eventCount = file.Count().GetValue();

	auto filteredFile = file.Filter(filter);
	auto filteredEventCount = filteredFile.Count().GetValue();

	filteredFile.Snapshot("dimu/dimu", filename.substr(0, filename.length() - 5) + "_filtered.root");

	std::cout << "Events before filter: " << eventCount << std::endl;
	std::cout << "Events after filter: " << filteredEventCount << " (" << (double)filteredEventCount/eventCount*100 << " %)" << std::endl;

	return;
}
