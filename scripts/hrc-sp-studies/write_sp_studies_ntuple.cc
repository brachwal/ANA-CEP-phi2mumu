#include <ROOT/RDataFrame.hxx>
#include <iostream>
#include <string>
#include <vector>
#include <array>

/*
	A script to filter exclusive events, with defining the:
	type = 0 for background
	type = 1 for signal
	based one the HRC chi2 discriminant only.
*/

const std::string cut_pid = "mum_isMuon&&mup_isMuon&&mum_ProbNNmu>0.2&&mup_ProbNNmu>0.2";
const std::string cut_trks = "nVeloTracks==2&&nLongTracks==2";
const std::string cut_spd = "nSPDHits<10";
const std::string cut_mass = "phi_MM>960 && phi_MM<1080";
const std::string cut_hrc_sig = "log_hrc_fom_v2<5.5";
const std::string cut_hrc_bgd = "log_hrc_fom_v2>9.5";

const std::string filter_selection = cut_pid + "&&" + cut_trks +"&&" + cut_spd + "&&" + cut_mass;

void write_sp_studies_ntuple(unsigned type,std::string filename){

	ROOT::EnableImplicitMT();

	//______________________________________________________________
	std::string type_name;
	std::string typed_selection;
	switch(type){
		case 0:
			typed_selection = filter_selection + "&&" + cut_hrc_bgd;
			type_name = "bgd";
			break;
		case 1:
			typed_selection = filter_selection + "&&" + cut_hrc_sig;
			type_name = "sig";
			break;	
	}

	//______________________________________________________________
	std::cout << "[INFO]:: Processing: " << filename << std::endl;

	ROOT::RDataFrame rdf("dimu/dimu", filename);
	auto eventCount = rdf.Count().GetValue();
	std::cout << "[INFO]:: Events before filter: " << eventCount << std::endl;

	auto rdf_filtered = rdf.Filter(typed_selection);
	auto filteredEventCount = rdf_filtered.Count().GetValue();
	
	std::cout << "[INFO]:: Events after filter: " << filteredEventCount << " (" << (double)filteredEventCount/eventCount*100 << " %)" << std::endl;

	//______________________________________________________________
	auto merge_quadrants = [](int q1, int q2, int q3, int q4 ) { return std::vector<int>{q1,q2,q3,q4};};
	//auto merge_quadrants = [](int q1, int q2, int q3, int q4 ) { return std::array<int,4>{q1,q2,q3,q4};};
	//auto merge_quadrants = [](int q1, int q2, int q3, int q4 ) { int station[4] = {q1,q2,q3,q4}; return std::move(station);};
	auto rdf_out = rdf_filtered.Define("phi_E","sqrt(phi_MM*phi_MM+phi_P*phi_P)")
				   			   .Define("B0",merge_quadrants,{"B00","B01","B02","B03"})
				   			   .Define("B1",merge_quadrants,{"B10","B11","B12","B13"})
				   			   .Define("B2",merge_quadrants,{"B20","B21","B22","B23"})
				   			   .Define("F1",merge_quadrants,{"F10","F11","F12","F13"})
				   			   .Define("F2",merge_quadrants,{"F20","F21","F22","F23"})
				   			   .Define("Y_diff","mum_Y-mup_Y");
	
	//______________________________________________________________
	std::vector<std::string> outBranchList{"phi_MM","phi_ETA","phi_Y","phi_P","phi_P2","phi_PT","phi_PT2"
										  ,"mum_P","mum_P2","mum_PT","mum_PT2","mum_ETA","mum_Y","mum_ProbNNmu"
										  ,"mup_P","mup_P2","mup_PT","mup_PT2","mup_ETA","mup_Y","Y_diff","mup_ProbNNmu"
										  ,"log_hrc_fom_v2", "log_hrc_fom_B_v2", "log_hrc_fom_F_v2"
										  , "B0", "B1", "B2", "F1", "F2"
										  ,"nB0_mean_Odd","nB0_mean_Even","nB0_rms_Odd","nB0_rms_Even"
										  ,"B0_mean_Odd","B0_mean_Even","B0_rms_Odd","B0_rms_Even"
										  ,"nB1_mean_Odd","nB1_mean_Even","nB1_rms_Odd","nB1_rms_Even"
										  ,"B1_mean_Odd","B1_mean_Even","B1_rms_Odd","B1_rms_Even"
										  ,"nB2_mean_Odd","nB2_mean_Even","nB2_rms_Odd","nB2_rms_Even"
										  ,"B2_mean_Odd","B2_mean_Even","B2_rms_Odd","B2_rms_Even"
										  ,"nF1_mean_Odd","nF1_mean_Even","nF1_rms_Odd","nF1_rms_Even"
										  ,"F1_mean_Odd","F1_mean_Even","F1_rms_Odd","F1_rms_Even"
										  ,"nF2_mean_Odd","nF2_mean_Even","nF2_rms_Odd","nF2_rms_Even"
										  ,"F2_mean_Odd","F2_mean_Even","F2_rms_Odd","F2_rms_Even"
										  };

    //______________________________________________________________										  
	auto newFileName = filename.substr(0, filename.length() - 5) +"_"+type_name +"_snapshot.root";
	rdf_out.Snapshot("dimu", newFileName,outBranchList);

	return;
}
