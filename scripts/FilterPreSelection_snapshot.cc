#include <ROOT/RDataFrame.hxx>
#include <iostream>
#include <string>

/*
	A script to filter and reduce NTuple size for further analysis.

*/

const std::string filter = "nVeloTracks < 3 && mum_isMuon && mup_isMuon";


void FilterPreSelection_snapshot(std::string filename){


	std::cout << filename << std::endl;

	ROOT::RDataFrame file("dimu/dimu", filename);
	auto eventCount = file.Count().GetValue();

	auto filteredFile = file.Filter(filter);
	auto filteredEventCount = filteredFile.Count().GetValue();

	std::vector<std::string> outBranchList;
	outBranchList.push_back("phi_MM");
	outBranchList.push_back("phi_ETA");
	outBranchList.push_back("phi_P");
	outBranchList.push_back("phi_PZ");
	outBranchList.push_back("phi_P2");
	outBranchList.push_back("phi_PT");
	outBranchList.push_back("mum_P");
	outBranchList.push_back("mum_P2");
	outBranchList.push_back("mum_PT");
	outBranchList.push_back("mum_PT2");
	outBranchList.push_back("mum_ETA");
	outBranchList.push_back("mum_Y");
	outBranchList.push_back("mup_P");
	outBranchList.push_back("mup_P2");
	outBranchList.push_back("mup_PT");
	outBranchList.push_back("mup_PT2");
	outBranchList.push_back("mup_ETA");
	outBranchList.push_back("mup_Y");
	outBranchList.push_back("mum_isMuon");
	outBranchList.push_back("mup_isMuon");
	outBranchList.push_back("mum_ProbNNmu");
	outBranchList.push_back("mup_ProbNNmu");
	outBranchList.push_back("nVeloTracks");
	outBranchList.push_back("nLongTracks");
	outBranchList.push_back("nSPDHits");

	filteredFile.Snapshot("dimu", filename.substr(0, filename.length() - 5) + "_filtered.root",outBranchList);

	std::cout << "Events before filter: " << eventCount << std::endl;
	std::cout << "Events after filter: " << filteredEventCount << " (" << (double)filteredEventCount/eventCount*100 << " %)" << std::endl;

	return;
}
