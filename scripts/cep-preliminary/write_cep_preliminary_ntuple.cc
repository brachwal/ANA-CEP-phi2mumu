#include <ROOT/RDataFrame.hxx>
#include <iostream>
#include <string>
#include <vector>

/*
	A script to filter exclusive events
	NOTE: do note realy on hrc separation!
*/

const std::string cut_pid = "mum_isMuon&&mup_isMuon&&mum_ProbNNmu>0.1&&mup_ProbNNmu>0.1";
const std::string cut_trks = "nVeloTracks==2&&nLongTracks==2&&nBackTracks==0";
const std::string cut_spd = "nSPDHits<10";
const std::string cut_mass = "phi_MM>960 && phi_MM<1080";

const std::string filter_selection = cut_pid + "&&" + cut_trks +"&&" + cut_spd + "&&" + cut_mass;

void write_cep_preliminary_ntuple(std::string filename){

	ROOT::EnableImplicitMT();

	std::cout << "[INFO]:: Processing: " << filename << std::endl;

	ROOT::RDataFrame rdf("dimu/dimu", filename);
	auto eventCount = rdf.Count().GetValue();
	std::cout << "[INFO]:: Events before filter: " << eventCount << std::endl;

	auto rdf_filtered = rdf.Filter(filter_selection);
	auto filteredEventCount = rdf_filtered.Count().GetValue();
	
	std::cout << "[INFO]:: Events after filter: " << filteredEventCount << " (" << (double)filteredEventCount/eventCount*100 << " %)" << std::endl;


	auto rdf_out = rdf_filtered.Define("phi_E","sqrt(phi_MM*phi_MM+phi_P*phi_P)")
							   .Define("Y_diff","mum_Y-mup_Y")
							   .Define("Pt_MuPair_sum","sqrt((mum_PX+mup_PX)*(mum_PX+mup_PX) + (mum_PY+mup_PY)*(mum_PY+mup_PY))");
	
	std::vector<std::string> outBranchList;
	outBranchList.push_back("phi_MM");
	outBranchList.push_back("phi_ETA");
	outBranchList.push_back("phi_Y");
	outBranchList.push_back("phi_P");
	outBranchList.push_back("phi_P2");
	outBranchList.push_back("phi_PT");
	outBranchList.push_back("phi_PT2");
	outBranchList.push_back("mum_P");
	outBranchList.push_back("mum_P2");
	outBranchList.push_back("mum_PT");
	outBranchList.push_back("mum_PT2");
	outBranchList.push_back("mum_ETA");
	outBranchList.push_back("mum_Y");
	outBranchList.push_back("mum_ProbNNmu");
	outBranchList.push_back("mup_P");
	outBranchList.push_back("mup_P2");
	outBranchList.push_back("mup_PT");
	outBranchList.push_back("mup_PT2");
	outBranchList.push_back("mup_ETA");
	outBranchList.push_back("mup_Y");
	outBranchList.push_back("Y_diff");
	outBranchList.push_back("mup_ProbNNmu");
	outBranchList.push_back("Pt_MuPair_sum");

	auto newFileName = filename.substr(0, filename.length() - 5) + "_cep_preliminary.root";
	rdf_out.Snapshot("dimu", newFileName,outBranchList);

	return;
}
