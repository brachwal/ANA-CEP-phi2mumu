#include <ROOT/RDataFrame.hxx>
#include <iostream>
#include <memory>
#include <string>
#include <vector>
#include <map>
#include "TCanvas.h"

/*
	A script to make CEP like preliminary histograms,
	Y_diff for dirrefere selections:
	1) basic ( as defined in write_cep_preliminary_ntuple.cc )
	2) muons pt cut: mum_PT>500 && mup_PT>500
	3) muons sum pt cut (Pt_MuPair_sum > 0.5 / 0.8 / 1.0 GeV)
	4) Phi_PT^2 cut (phi_PT2<1000000 GeV^2)
*/

using Rdf = ROOT::RDataFrame;
using RdfFiltered = ROOT::RDF::RInterface<ROOT::Detail::RDF::RJittedFilter, void>;
using RdfSharedPtr = std::shared_ptr<Rdf>;
using RdfFilteredSharedPtr = std::shared_ptr<RdfFiltered>;
using RResultPtrTH1D = ROOT::RDF::RResultPtr<TH1D>;

void write_cep_preliminary_histograms(std::string filename){

	ROOT::EnableImplicitMT();
	std::cout << "[INFO]:: Processing: " << filename << std::endl;

	// Basic dataframe
	auto cep_selected_rdf = std::make_shared<Rdf>("dimu", filename);

	//_____________________________________________________________________________
	auto filterRdf = [=](const std::string& selection){
		return std::make_shared<RdfFiltered>(cep_selected_rdf->Filter(selection));
	};

	//_____________________________________________________________________________
	const std::string muons_pt_sel = "mum_PT>500&&mup_PT>500";
	const std::string phi_pt_sel = "phi_PT2<1000000";
	const std::string mu_pair_pt_sum500_sel = "Pt_MuPair_sum > 500";
	const std::string mu_pair_pt_sum800_sel = "Pt_MuPair_sum > 800";
	const std::string mu_pair_pt_sum1000_sel = "Pt_MuPair_sum > 1000";

	//_____________________________________________________________________________
	std::map<unsigned,RdfFilteredSharedPtr> selectedRdf;
	selectedRdf[1] 	= filterRdf( muons_pt_sel );
	selectedRdf[2] 	= filterRdf( muons_pt_sel +"&&"+ phi_pt_sel );
	selectedRdf[3] 	= filterRdf( mu_pair_pt_sum500_sel );
	selectedRdf[4] 	= filterRdf( mu_pair_pt_sum800_sel );
	selectedRdf[5] 	= filterRdf( mu_pair_pt_sum1000_sel );
	

	//_____________________________________________________________________________
	std::map<unsigned,RResultPtrTH1D> selectedTH1D;
	selectedTH1D[0] = cep_selected_rdf->Histo1D({"Ydiff_0","",100,-5.,5.},"Y_diff");
	for(unsigned i=1; i<selectedRdf.size()+1;++i)
		selectedTH1D[i] = selectedRdf[i]->Histo1D({"Y_diff_","",100,-5.,5.},"Y_diff");
	
	cout<< "INFO:: selected number of histograms " << selectedTH1D.size() <<std::endl;
	//_____________________________________________________________________________
	// Build manualy all titles
	auto configHist = [](RResultPtrTH1D& hist, int color, std::string title){
		hist->SetLineWidth(2);
		hist->SetLineColor(color);
		hist->SetTitle(TString(title));
		hist->SetStats(false);
	};

	std::string y_diff_axes = "#mu^{-}_{Y}-#mu^{+}_{Y};Normalized (au.)";
	configHist(selectedTH1D[0],  kRed, 		"Muons Y diff (basic selection);"+y_diff_axes);
	configHist(selectedTH1D[1],  kGreen, 	"Muons Y diff (muons_pt > 0.5 GeV);"+y_diff_axes);
	configHist(selectedTH1D[2],  kBlue, 	"Muons Y diff (muons_pt > 0.5 GeV & phi_pt2<10e6);"+y_diff_axes);
	configHist(selectedTH1D[3],  kOrange+1, "Muons Y diff (mu pair pt sum > 0.5 GeV);"+y_diff_axes);
	configHist(selectedTH1D[4],  kOrange+3, "Muons Y diff (mu pair pt sum > 0.8 GeV);"+y_diff_axes);
	configHist(selectedTH1D[5],  kOrange+5, "Muons Y diff (mu pair pt sum > 1.0 GeV);"+y_diff_axes);
	

	//_____________________________________________________________________________
	auto c1 = new TCanvas("cYdiff","Y_diff",700,600);
	selectedTH1D[0]->DrawNormalized();
	for(unsigned i=1; i<selectedTH1D.size();++i)
		selectedTH1D[i]->DrawNormalized("SAME");

	//_____________________________________________________________________________
	auto c2 = new TCanvas("cMuSumPT","cMuSumPT",700,600);
	auto muSumPT = cep_selected_rdf->Histo1D({"Pt_MuPair_sum","",100,0.,2000.},"Pt_MuPair_sum");
	configHist(muSumPT, kGreen,"MuPair_sumPT;p_{t,pair};Normalized (au.)");
	muSumPT->DrawNormalized();

	//_____________________________________________________________________________
	auto outfile = TFile("CEP-Phi2MuMu-Preliminary-Histograms.root", "RECREATE");
	auto list = TList();
	for(auto& iter : selectedTH1D)
		list.Add(iter.second.GetPtr());
	list.Write();
	outfile.Close();

	return;
}
