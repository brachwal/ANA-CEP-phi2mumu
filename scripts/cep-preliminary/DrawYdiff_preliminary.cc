//
// Note: You can run this script on ROOT version that gives RDataFrame!
//       root -l  DrawYdif_preliminary.C

using TH1DPTR = ROOT::RDF::RResultPtr<TH1D>;
using RDF = ROOT::RDataFrame;

int DrawYdiff_preliminary(){

	auto configHist = [](TH1DPTR h, int color){
		//h->GetXaxis()->SetRangeUser(0, 5);
		h->GetXaxis()->SetTitle("Y diff");
		h->GetYaxis()->SetTitle("Normalized");
		h->SetLineColor(color);
		h->SetLineWidth(2);
		// h->SetMarkerColor(color);
		// h->SetMarkerSize(1);
		// h->SetMarkerStyle(8);
		h->SetStats(false);
	};

	auto buildLegend = [](std::vector<TH1*>& hists, const TString& title, const std::vector<TString>& entries){
		auto legend = new TLegend(.6,.6,.9,.9);
		//legend->AddEntry((TObject*)0, title, "");
		for(int i=0; i<hists.size();++i)
			legend->AddEntry(hists.at(i), entries.at(i), "l");
		return legend;
	};

	auto selectRDF = [](std::string fName){
		std::string fileLocation = "/eos/lhcb/user/b/brachwal/DATA/Analysis/CEP/LowDiMu";
		std::string treeName = "dimu/dimu";
		std::string rootFile = fileLocation+"/"+fName;
		RDF dF(treeName, rootFile);
		return dF;
	};

	auto selectHist = [](RDF& rdf){
		return rdf.Filter({"nVeloTracks < 3"})
				  .Filter({"mum_isMuon && mup_isMuon"})
				  .Define("Ydiff", [](double mumY, double mupY) { return mumY - mupY; }, {"mum_Y", "mup_Y"})
				  .Histo1D({"","",100,-5,5},"Ydiff");
	};

	auto selectHistPT = [](RDF& rdf){
		return rdf.Filter({"nVeloTracks < 3"})
				  .Filter({"mum_isMuon && mup_isMuon && mum_PT > 500 && mup_PT > 500"})
				  .Define("Ydiff", [](double mumY, double mupY) { return mumY - mupY; }, {"mum_Y", "mup_Y"})
				  .Histo1D({"","",100,-5,5},"Ydiff");
	};

	std::vector<TH1*> resultingHists;
	std::vector<TString> resultingHistsLegendEntries;
	
	/////////////////////////////////////////////////////////////////////////////////////////	
	/// Compile results...
	/////////////////////////////////////////////////////////////////////////////////////////	
	
	// std::string fileName16 = "LowDimu_2016_NEW_test.root";
	// auto rdf16 = selectRDF(fileName16);
	// auto h16 = selectHist(rdf16);
	// resultingHistsLegendEntries.push_back("Data 2016");
	// configHist(h16,3);

	// std::string fileName17 = "LowDimu_2017_NEW_test.root";
	// auto rdf17 = selectRDF(fileName17);
	// auto h17 = selectHist(rdf17);
	// resultingHistsLegendEntries.push_back("Data 2017");
	// configHist(h17,2);

	// std::string fileName18 = "LowDimu_2018_NEW_test.root";
	// auto rdf18 = selectRDF(fileName18);
	// auto h18 = selectHist(rdf18);
	// resultingHistsLegendEntries.push_back("Data 2018");
	// configHist(h18,4);

	std::string fileName1718 = "LowDimu_201718_NEW_test.root";
	auto rdf1718 = selectRDF(fileName1718);
	auto h1718 = selectHist(rdf1718);
	auto rdf1718_pt = selectRDF(fileName1718);
	auto h1718_pt = selectHistPT(rdf1718_pt);
	resultingHistsLegendEntries.push_back("Data run 2 (17+18)");
	resultingHistsLegendEntries.push_back("Data run 2 (17+18), mu pt > 0.5 GeV");
	configHist(h1718,4);
	configHist(h1718_pt,3);

	auto c = new TCanvas();		 
	resultingHists.push_back(h1718->DrawNormalized(""));
	resultingHists.push_back(h1718_pt->DrawNormalized("SAME"));
	auto legend = buildLegend(resultingHists,"Y diff observable",resultingHistsLegendEntries);
	legend->Draw("SAME");
	c->SetLogy();

	
	return 0;
}
