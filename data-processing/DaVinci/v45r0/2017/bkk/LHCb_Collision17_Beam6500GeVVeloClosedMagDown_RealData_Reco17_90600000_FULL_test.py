#-- GAUDI jobOptions generated on Mon Jul  1 14:31:48 2019
#-- Contains event types : 
#--   90600000 - 19721 files - 2717230962 events - 51419.81 GBytes

#--  Extra information about the data processing phases:

#--  Processing Pass: '/Real Data/Reco17' 

#--  StepId : 131873 
#--  StepName : FULL-Reco17 (cond-20170724 for LOWMULT and Nobias after MD) 
#--  ApplicationName : Brunel 
#--  ApplicationVersion : v52r5 
#--  OptionFiles : $APPCONFIGOPTS/Brunel/DataType-2017.py 
#--  DDDB : dddb-20150724 
#--  CONDDB : cond-20170724 
#--  ExtraPackages : AppConfig.v3r323;SQLDDDB.v7r10 
#--  Visible : Y 

#--  Processing Pass: '/Real Data/Reco17' 

#--  StepId : 131531 
#--  StepName : FULL-Reco17-cond-20170510 
#--  ApplicationName : Brunel 
#--  ApplicationVersion : v52r5 
#--  OptionFiles : $APPCONFIGOPTS/Brunel/DataType-2017.py 
#--  DDDB : dddb-20150724 
#--  CONDDB : cond-20170510 
#--  ExtraPackages : AppConfig.v3r323;SQLDDDB.v7r10 
#--  Visible : Y 

#--  Processing Pass: '/Real Data/Reco17' 

#--  StepId : 132054 
#--  StepName : FULL-Reco17-cond-20170724 (for LOWMULT and Nobias) 
#--  ApplicationName : Brunel 
#--  ApplicationVersion : v52r6p1 
#--  OptionFiles : $APPCONFIGOPTS/Brunel/DataType-2017.py 
#--  DDDB : dddb-20150724 
#--  CONDDB : cond-20170724 
#--  ExtraPackages : AppConfig.v3r323;SQLDDDB.v7r10 
#--  Visible : Y 

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00066582/0000/00066582_00004332_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00064353/0000/00064353_00000477_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00066582/0000/00066582_00002541_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00066582/0000/00066582_00001711_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00066582/0001/00066582_00010351_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00064796/0000/00064796_00003223_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00066582/0000/00066582_00001870_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00066582/0000/00066582_00006024_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00066582/0000/00066582_00006851_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00066582/0000/00066582_00006682_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00064796/0000/00064796_00004381_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00064353/0000/00064353_00001708_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00064796/0000/00064796_00004333_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00064796/0000/00064796_00000336_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00066582/0000/00066582_00005504_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00064353/0000/00064353_00000108_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00064796/0000/00064796_00004464_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00066582/0000/00066582_00000574_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00066582/0000/00066582_00006298_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00066582/0000/00066582_00002691_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00066582/0000/00066582_00007488_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00066582/0000/00066582_00008295_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00064353/0000/00064353_00000223_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00066582/0001/00066582_00013547_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00066582/0001/00066582_00011921_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00064796/0000/00064796_00000174_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00066582/0000/00066582_00002871_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00066582/0001/00066582_00011516_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00066582/0000/00066582_00009351_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00064353/0000/00064353_00001402_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00066582/0001/00066582_00010280_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00066582/0000/00066582_00006628_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00064796/0000/00064796_00000694_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00066582/0001/00066582_00013514_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00064796/0000/00064796_00000958_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00066582/0000/00066582_00007138_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00064796/0000/00064796_00000348_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00064796/0000/00064796_00002277_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00066582/0000/00066582_00001673_1.full.dst',
'LFN:/lhcb/LHCb/Collision17/FULL.DST/00064353/0000/00064353_00000155_1.full.dst',
], clear=True)
