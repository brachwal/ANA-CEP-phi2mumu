from Gaudi.Configuration import *
from Configurables import DaVinci
# Tuples
from Configurables import EventTuple, DecayTreeTuple
# Trigger
from Configurables import L0TriggerTisTos, TriggerTisTos
# Tuple tools
from Configurables import TupleToolStripping, TupleToolHerschel, TupleToolTrigger
from Configurables import LoKi__Hybrid__TupleTool

# Selection tools
from DecayTreeTuple.Configuration import *
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence, DataOnDemand
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles

#=============================#
#=== List of trigger lines ===#
#=============================#

triggerListAll = [ "Hlt1NoPVPassThroughDecision",
                   "Hlt1CEPVeloCutDecision", 
                   "Hlt1NoBiasNonBeamBeamDecision",
                   "Hlt2NoBiasNonBeamBeamDecision",
                   "Hlt2LowMultTechnical_MinBiasDecision"]

#=========================#
#=== Set up EventTuple ===#
#=========================#

from Configurables import EventTuple
etuple = EventTuple("EventTuple")
etuple.TupleName = "EventTuple"
etuple.ToolList += ["TupleToolEventInfo",
                    "TupleToolL0Data",
                    "TupleToolFillingScheme"]

tts = etuple.addTupleTool("TupleToolStripping")
tts.StrippingList = ["StrippingLowMultNonBeamBeamNoBiasLineDecision"]

et_ttt=etuple.addTupleTool("TupleToolTrigger")
et_ttt.VerboseL0=True
et_ttt.VerboseHlt1=True
et_ttt.VerboseHlt2=True
et_ttt.TriggerList=triggerListAll
et_tth=etuple.addTupleTool("TupleToolHerschel")
et_tth.DigitsLocation = "Raw/HC/Digits"
# et_tth.DigitsLocation = "Raw/HC/CorrectedDigits"
et_tth.CrateB = 0
et_tth.CrateF = 1

et_tth.MakeFOM = False    # No data to create such
et_tth.WriteCalib = False # No data to extarct this way

#=========================#
#=== Configure DaVinci ===#
#=========================#
DaVseqList=[]
# Tags
from Configurables import CondDB
db = CondDB ( LatestGlobalTagByDataType = "2017" )

DaVinci().Lumi = False
DaVinci().Simulation = False
DaVinci().InputType = 'DST'
DaVinci().TupleFile= "HRC_PEDESTALS.root"

DaVinci().EvtMax = -1
DaVinci().DataType = "2017"
DaVinci().PrintFreq = 500

# Filter events
from PhysConf.Filters import LoKi_Filters

NonBeamBeamHlt1Filter = LoKi_Filters(
                        HLT1_Code="HLT_PASS('Hlt1NoBiasNonBeamBeamDecision')") #empty-empty events

NonBeamBeamHlt2Filter = LoKi_Filters(
                        HLT2_Code="HLT_PASS('Hlt2NoBiasNonBeamBeamDecision')") #empty-empty events

LongTrackFilter = LoKi_Filters (
        VOID_Code = "( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < 6 )",
        VOID_Preambulo =  ["from LoKiTracks.decorators import *"]
)

# Construct code to run
from Configurables import HCRawBankDecoder
decoder = HCRawBankDecoder()
DaVinci().UserAlgorithms += [ decoder ]; # 
DaVinci().EventPreFilters = NonBeamBeamHlt1Filter.filters('NonBeamBeamHlt1Filter')
DaVinci().EventPreFilters+= NonBeamBeamHlt2Filter.filters('NonBeamBeamHlt2Filter')
DaVinci().EventPreFilters+= LongTrackFilter.filters('LongTrackFilter')

DaVinci().UserAlgorithms += [ etuple ];




