#==============================================================================
#
DVver  = "v45r0"
DVopts = "PhiDiMu_DATA_2017.py"

job = "1"

#InputFile = "LHCb_Collision17_Beam6500GeVVeloClosedMagDown_RealData_Reco17_90600000_FULL_test.py"
InputFile = "LHCb_Collision17_Beam6500GeVVeloClosedMagDown_RealData_Reco17_90600000_FULL_v{0}.py".format(job)
#InputFile = "LHCb_Collision17_Beam6500GeVVeloClosedMagUp_RealData_Reco17_90600000_FULL_v{0}.py".format(job)
OutputFile = "*.root"

#==============================================================================

#j = Job(name='test17')
j = Job(name='Phi2MuMu17MDj{0}'.format(job))

myApp = GaudiExec()
myApp.platform = 'x86_64-slc6-gcc8-opt'
myApp.directory = '/afs/cern.ch/user/b/brachwal/cmtuser/DaVinciDev_{0}'.format(DVver)

appOpts =  '/afs/cern.ch/user/b/brachwal/workspace/Analysis/CEP/ANA-CEP-phi2mumu/data-processing/DaVinci/{0}/2017/{1}'.format(DVver,DVopts)
appInput = '/afs/cern.ch/user/b/brachwal/workspace/Analysis/CEP/ANA-CEP-phi2mumu/data-processing/DaVinci/{0}/2017/bkk/{1}'.format(DVver,InputFile)

#==============================================================================

j.application = myApp
j.application.options = [appOpts]
# inputLFN
j.application.readInputData(appInput)

j.splitter = SplitByFiles ( filesPerJob = 10, maxFiles = -1)
j.backend = Dirac()

#j.backend = Local()
#j.outputfiles = [LocalFile(namePattern=OutputFile)]
j.outputfiles = [DiracFile(namePattern=OutputFile,locations=["CERN-USER"])]
j.submit()
#==============================================================================
