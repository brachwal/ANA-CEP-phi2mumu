from Gaudi.Configuration import *
from Configurables import DaVinci
# Tuples
from Configurables import EventTuple, DecayTreeTuple, MCDecayTreeTuple
# Trigger
from Configurables import L0TriggerTisTos, TriggerTisTos
# Tuple tools
from Configurables import TupleToolMCTruth, TupleToolMCBackgroundInfo, TupleToolGeneration, TupleToolPropertime, TupleToolTISTOS, TupleToolStripping, TupleToolHerschel, TupleToolTrigger
from Configurables import MCTupleToolAngles, MCTupleToolDecayType, MCTupleToolHierarchy, MCTupleToolInteractions, MCTupleToolKinematic, MCTupleToolPrimaries
from Configurables import MCTupleToolEventType, MCTupleToolPID, MCTupleToolReconstructed
from Configurables import LoKi__Hybrid__TupleTool, TupleToolDecay, TupleToolTrackIsolation
from Configurables import TupleToolVELOClusters, TupleToolVeloTrackClusterInfo
# Selection tools
from DecayTreeTuple.Configuration import *
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence, DataOnDemand
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
#=======================#

#=== Make candidates ===#
#=======================#
from StandardParticles import StdAllNoPIDsMuons as _stdNoPIDMuons

combine_LowMuMu = CombineParticles()
combine_LowMuMu.DecayDescriptor = "phi(1020) -> mu- mu+"
combine_LowMuMu.DaughtersCuts  = { "mu-" : "(PT>0.*MeV) & (TRCHI2DOF<5) & (ISLONG) & (ISMUON)",
                                   "mu+" : "(PT>0.*MeV) & (TRCHI2DOF<5) & (ISLONG) & (ISMUON)" }
combine_LowMuMu.CombinationCut = "(AM>0) & (AM <1200) & (AMAXDOCA('')<0.3*mm)"
combine_LowMuMu.MotherCut = "(P > 0.*MeV)"

LowDiMu = Selection ( name="SelLowDiMuon",
                      Algorithm = combine_LowMuMu,
                      RequiredSelections = [_stdNoPIDMuons] )

seqLowDiMu = SelectionSequence( "seq_LMR2DiMu", TopSelection = LowDiMu);

#=============================#
#=== List of trigger lines ===#
#=============================#

mystrips   = [  "StrippingLowMultNonBeamBeamNoBiasLineDecision",
                "StrippingLowMultMuonLineDecision",
                "StrippingLowMultDiMuonLineDecision" ]

triggerListAll = [ "L0Muon,lowMultDecision",
                   "L0DiMuon,lowMultDecision",
                   "Hlt1NoPVPassThroughDecision",
                   "Hlt1CEPVeloCutDecision", 
                   "Hlt1NoBiasNonBeamBeamDecision",
                   "Hlt2NoBiasNonBeamBeamDecision",
                   "Hlt2LowMultTechnical_MinBiasDecision",
                   "Hlt2LowMultMuonDecision",
                   "Hlt2LowMultDiMuonDecision"]

#==============================#
#=== Set up DecayTreeTuples ===#
#==============================#
DiMuTuple = DecayTreeTuple("dimu");
DiMuTuple.TupleName = "dimu"
DiMuTuple.Inputs = [LowDiMu.outputLocation()]
DiMuTuple.Decay = "(phi(1020) -> ^mu+ ^mu-)"
DiMuTuple.addBranches({
       "mup"     :    "phi(1020) -> ^mu+  mu-"
      ,"mum"     :    "phi(1020) ->  mu+ ^mu-"
      ,"phi"    :    "phi(1020) -> mu+ mu-"
})

LoKi_Dimuon=DiMuTuple.phi.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Dimuon")
LoKi_Dimuon.Variables =  {
        "cosP1" : "LV01"
      , "cosP2" : "LV02"
      , "VCHI2" : "VFASPF(VCHI2/VDOF)"
      , "VCHI2_NDOF" : "VFASPF(VCHI2/VDOF)"
      , "DOCA" : "DOCA(1,2)"
      , "DOCACHI2" : "DOCACHI2(1,2)"
      , "ETA"  : "ETA"
      , "Y" : "log((E+PZ)/(E-PZ))/2."
      , "P2" : "P*P"
      , "PT2" : "PT*PT" 
      }

def mySharedConf(branch):
  atool=branch.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Mu")
  atool.Variables =  {
      "ETA"  : "ETA"
    , "Y" : "log((E+PZ)/(E-PZ))/2." 
    , "P2" : "P*P" 
    , "PT2" : "PT*PT" 
  }

mySharedConf(DiMuTuple.mum)
mySharedConf(DiMuTuple.mup)


ntpList=[DiMuTuple]

#======================#
#=== Load nTuples =====#
#======================#
for ntp in ntpList:
  ntp.ToolList += [ "TupleToolEventInfo",
                    "TupleToolAngles",
                    "TupleToolGeometry",
                    "TupleToolKinematic",
                    "TupleToolL0Data",
                    "TupleToolPid",
                    "TupleToolANNPID",
                    "TupleToolPrimaries",
                    "TupleToolParticleStats",
                    "TupleToolPhotonInfo",
                    "TupleToolRecoStats",
                    "TupleToolTrackInfo",
                    "TupleToolVELOClusters",
                    "TupleToolVeloTrackClusterInfo",
                    "TupleToolFillingScheme"
                    ]


  ttt=ntp.addTupleTool("TupleToolTrigger");
  ttt.VerboseL0=True
  ttt.VerboseHlt1=True
  ttt.VerboseHlt2=True
  ttt.FillL0 = True
  ttt.TriggerList=triggerListAll
  tth=ntp.addTupleTool("TupleToolHerschel")
  tth.DigitsLocation = "Raw/HC/Digits"
  #tth.DigitsLocation = "Raw/HC/CorrectedDigits"   # <= HCDigitCorrector
  tth.CrateB = 0
  tth.CrateF = 1
  
  tth.MakeFOM = True    # default anyway
  tth.WriteCalib = True # default anyway

  ntp.mup.ToolList += [ "TupleToolTISTOS/muPlusTISTOS" ]
  ntp.mup.addTool( TupleToolTISTOS, name = "muPlusTISTOS" )
  ntp.mup.muPlusTISTOS.Verbose = True
  ntp.mup.muPlusTISTOS.TriggerList = triggerListAll

  ntp.mum.ToolList += [ "TupleToolTISTOS/muMinusTISTOS" ]
  ntp.mum.addTool( TupleToolTISTOS, name = "muMinusTISTOS" )
  ntp.mum.muMinusTISTOS.Verbose = True
  ntp.mum.muMinusTISTOS.TriggerList = triggerListAll

  tts = ntp.addTupleTool("TupleToolStripping")
  tts.StrippingList = mystrips

#=========================#
#=== Configure DaVinci ===#
#=========================#
DaVseqList=[]
# Tags
from Configurables import CondDB
db = CondDB ( LatestGlobalTagByDataType = "2016" )

DaVinci().Lumi = True
DaVinci().Simulation = False
DaVinci().InputType = 'DST'
DaVinci().TupleFile= "LowMultDimuon.root"

DaVinci().EvtMax = -1
DaVinci().DataType = "2016"
DaVinci().PrintFreq = 500

# Filter events
from PhysConf.Filters import LoKi_Filters

MuonStripFilter = LoKi_Filters(
                        STRIP_Code="HLT_PASS('StrippingLowMultMuonLineDecision') | \
                                    HLT_PASS('StrippingLowMultDiMuonLineDecision')" )

LongTrackFilter = LoKi_Filters (
        VOID_Code = "( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < 6 ) & \
                     ( recSummaryTrack(LHCb.RecSummary.nVeloTracks, TrVELO) < 6 )" ,
        VOID_Preambulo =  ["from LoKiTracks.decorators import *"]
)

# Construct code to run
from Configurables import HCRawBankDecoder
decoder = HCRawBankDecoder()
DaVinci().EventPreFilters = MuonStripFilter.filters('MuonStripFilter')+LongTrackFilter.filters('LongTrackFilter')
DaVinci().UserAlgorithms += [ decoder, seqLowDiMu.sequence(), DiMuTuple ];
