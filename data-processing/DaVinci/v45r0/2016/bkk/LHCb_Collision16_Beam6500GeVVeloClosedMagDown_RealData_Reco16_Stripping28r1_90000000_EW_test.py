#-- GAUDI jobOptions generated on Sun Jan  6 10:18:39 2019
#-- Contains event types : 
#--   90000000 - 13259 files - 2755131302 events - 44839.14 GBytes

#--  Extra information about the data processing phases:
#--  Processing Pass Step-132873 

#--  StepId : 132873 
#--  StepName : Stripping28r1-Merging-DV-v41r4p4-AppConfig-v3r345 
#--  ApplicationName : DaVinci 
#--  ApplicationVersion : v41r4p4 
#--  OptionFiles : $APPCONFIGOPTS/Merging/DV-Stripping-Merging.py;$APPCONFIGOPTS/Persistency/Compression-LZMA-4.py 
#--  DDDB : dddb-20150724 
#--  CONDDB : cond-20161004 
#--  ExtraPackages : AppConfig.v3r345;SQLDDDB.v7r10 
#--  Visible : N 

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles(['LFN:/lhcb/LHCb/Collision16/EW.DST/00069527/0000/00069527_00000008_1.ew.dst',
'LFN:/lhcb/LHCb/Collision16/EW.DST/00069527/0000/00069527_00000015_1.ew.dst',
'LFN:/lhcb/LHCb/Collision16/EW.DST/00069527/0000/00069527_00000017_1.ew.dst',
'LFN:/lhcb/LHCb/Collision16/EW.DST/00069527/0000/00069527_00000033_1.ew.dst',
'LFN:/lhcb/LHCb/Collision16/EW.DST/00069527/0000/00069527_00000048_1.ew.dst',
'LFN:/lhcb/LHCb/Collision16/EW.DST/00069527/0000/00069527_00000053_1.ew.dst',
'LFN:/lhcb/LHCb/Collision16/EW.DST/00069527/0000/00069527_00000058_1.ew.dst',
'LFN:/lhcb/LHCb/Collision16/EW.DST/00069527/0000/00069527_00000073_1.ew.dst',
'LFN:/lhcb/LHCb/Collision16/EW.DST/00069527/0000/00069527_00000081_1.ew.dst',
'LFN:/lhcb/LHCb/Collision16/EW.DST/00069527/0000/00069527_00000094_1.ew.dst',
'LFN:/lhcb/LHCb/Collision16/EW.DST/00069527/0000/00069527_00000104_1.ew.dst',
'LFN:/lhcb/LHCb/Collision16/EW.DST/00069527/0000/00069527_00000108_1.ew.dst',
'LFN:/lhcb/LHCb/Collision16/EW.DST/00069527/0000/00069527_00000116_1.ew.dst',
'LFN:/lhcb/LHCb/Collision16/EW.DST/00069527/0000/00069527_00000129_1.ew.dst',
'LFN:/lhcb/LHCb/Collision16/EW.DST/00069527/0000/00069527_00000133_1.ew.dst'
], clear=True)
